class ResponseOutage():
    """
    ResponseOutage is a helper class that takes in a ResponseData object, and parses potential webwork outages from that
    data.
    """

    def __init__(self, responsedata, threshold, delay):
        """
        Initialize a ResponseOutage object
        :param responsedata: An object of ResponseData type
        :param threshold: The threshold in seconds to consider a WebWork outage.
        :param delay: How long to show an outage in WebWork Status' UI after the outage ended in seconds.
        """

        self.responsedata = responsedata
        self.threshold = threshold
        self.delay = delay
        self.outage_status = ""
        self.outage_zones = []
        self.outage_starttime = 0
        self.outage_endtime = 0

        self.__processoutagesdata()
        self.__processoutagezones()

    def __processoutagesdata(self):
        """
        Processes the outages data from the given responsedata object. This is part 1 of processing new data, and
        will put all times that WebWork was experiencing an outage (according to the threshold) in outage zones.
        After calling this method, you should call processoutagezones.
        :return: Nothing
        """
        self.outage_zones = []

        for key in self.responsedata.export():
            try:
                if (self.responsedata.get(key, average=True) >= self.threshold) \
                        and (self.responsedata.get(key) >= self.threshold):
                    self.outage_zones.append(int(key))
            except KeyError:
                continue

        self.outage_zones.sort()

    def __processoutagezones(self):
        """
        Processes the outage zones data, producing the outage status, duration, and timing.
        :return: Nothing
        """
        try:
            self.outage_starttime = self.outage_zones[0]
            self.outage_endtime = self.outage_zones[0]
        except KeyError:
            pass
        except IndexError:
            pass

        for i in range(1, len(self.outage_zones)):
            if self.outage_zones[i] - self.responsedata.interval() == self.outage_endtime:
                self.outage_endtime = self.outage_zones[i]
            else:
                self.outage_starttime = self.outage_zones[i]
                self.outage_endtime = self.outage_zones[i]

    def zonereturn(self):
        """
        Returns the raw outage zones array.
        :return: An array containing UNIX timestamps of when WebWork was down, according to the given threshold.
        """

        return self.outage_zones

    def outagereturn(self):
        """
        Returns data about the outage.
        :return: 4 variables, in order:
        The status of the outage as a string (blank: no outage, true: current outage, past: recently completed outage)
        The duration of the most recent outage (will be 0 if there was no recent outage)
        The starting time of the most recent outage as a UNIX timestamp (will be 0 if there was no recent outage)
        The ending time of the most recent outage as a UNIX timestamp (will be 0 if there was no recent outage)
        """
        outage_status = ""
        if self.outage_endtime == self.responsedata.latestkey():
            outage_status = "true"
        elif self.outage_endtime + self.delay >= self.responsedata.latestkey():
            outage_status = "past"

        outage_duration = int((self.outage_endtime - self.outage_starttime) / 60)

        return outage_status, outage_duration, self.outage_starttime, self.outage_endtime
