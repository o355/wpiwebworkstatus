from flask import Flask, render_template, jsonify, request
from ResponseData import ResponseData
from ResponseOutage import ResponseOutage
from datetime import datetime


app = Flask(__name__)

validservers = ["wwork", "wwserver"]

def statuscalc(lastresponse):
    wwstatus = ""
    status = ""
    if lastresponse < 5:
        # up/positive
        wwstatus = "up"
        status = "positive"
    elif lastresponse < 10:
        # slow/intermediary
        wwstatus = "slow"
        status = "intermediary"
    elif lastresponse < 20:
        # very slow/negative
        wwstatus = "very slow"
        status = "negative"
    elif lastresponse <= 120:
        # extremely slow/negative
        wwstatus = "extremely slow"
        status = "negative"
    else:
        # down/negative
        wwstatus = "down"
        status = "negative"

    return wwstatus, status


@app.route("/", methods=['GET'])
def main_route():
    
    data = ResponseData(url="https://wwserver.wpi.edu/webwork2", timeout=120, prunetime=259500, filename="responsedata-wwserver.json", avgwindow=7)
    outage = ResponseOutage(responsedata=data, threshold=5, delay=10800)


    newest_time = data.latestkey()
    next_responsetime = newest_time + 300

    wwstatus, status = statuscalc(data.get(latest=True))
    try:
        avglastresponsetime = '{:.2f}'.format(round(data.get(latest=True, average=True), 2)) + " seconds"
    except KeyError:
        avglastresponsetime = "Temporarily unavailable"

    lastresponsetime = data.get(latest=True)
    lastresponsetext_lowerrange = int(round(lastresponsetime, 0) * 0.5)
    if (data.get(latest=True, average=True) > lastresponsetime) and (data.get(latest=True, average=True) != 121):
        lastresponsetext_higherrange = (int(round(data.get(latest=True, average=True), 0) * 2))
    else:
        lastresponsetext_higherrange = (int(round(lastresponsetime, 0) * 2))

    if lastresponsetext_lowerrange < 0:
        lastresponsetext_lowerrange = 0

    if lastresponsetext_higherrange < 2:
        lastresponsetext_higherrange = 2

    lastresponsetext = str(lastresponsetext_lowerrange) + " - " + str(lastresponsetext_higherrange)

    latest_dtobj = datetime.fromtimestamp(newest_time)

    outage_status, outage_duration, outage_starttime, outage_endtime = outage.outagereturn()
    outage_endtime = outage_endtime + 300
    outage_duration = outage_duration + 5


    outage_starttime_prefill = datetime.fromtimestamp(outage_starttime).strftime("%-m/%-d, %l:%M %p")
    outage_endtime_prefill = datetime.fromtimestamp(outage_endtime).strftime("%-m/%-d, %l:%M %p")

    return render_template("page.html", wwstatus=wwstatus, lastresponse=lastresponsetext,
                           lastresponse_actual='{:.2f}'.format(round(lastresponsetime, 2)),
                           lastupdated=latest_dtobj.strftime("%B %-d, %Y at %l:%M %p"),
                           timestamp_cur=newest_time,
                           status=status,
                           fulldata=data.export(),
                           fulldata_avg=data.export(average=True),
                           avgresponse_actual=avglastresponsetime,
                           next_responsetime=next_responsetime,
                           outage=outage_status,
                           outage_starttime_prefill=outage_starttime_prefill,
                           outage_endtime_prefill=outage_endtime_prefill,
                           outage_duration=outage_duration,
                           outage_starttime=outage_starttime,
                           outage_endtime=outage_endtime)


@app.route("/api/v1/outageZones", methods=['GET'])
def outagezones():
    data = ResponseData(url="https://wwserver.wpi.edu/webwork2", timeout=120, prunetime=259500,
                        filename="responsedata-wwserver.json", avgwindow=7)
    outage = ResponseOutage(responsedata=data, threshold=5, delay=10800)

    return str(outage.zonereturn())


@app.route("/api/v1/siriShortcut", methods=['GET'])
def siriShortcut():

    data = ResponseData(url="https://wwserver.wpi.edu/webwork2", timeout=300, prunetime=259500,
                        filename="responsedata-wwserver.json", avgwindow=7)
    wwstatus, icon = statuscalc(data.get(latest=True))

    response = {"apiversion": "v3.0.0", wwstatus: True, "responsetime": round(data.get(latest=True), 2)}
    response = jsonify(response)
    response.headers.add("Access-Control-Allow-Origin", "*")

    return response

if __name__ == "__main__":
    app.run()