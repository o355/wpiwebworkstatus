import requests
import time
import json


class ResponseMeasure:
    """
    ResponseMeasure measures the response time of a website, and dumps results with a UNIX timestamp to a specified
    JSON file.
    """
    def __init__(self, url, filename, timeout, prunetime=0, floor=True):
        """
        Construct a new ResponseMeasure object.
        :param url: The URL of the website being requested.
        :param filename: The filename to dump the JSON data to.
        :param timeout: The timeout duration for the request. Requests that fail (or truly timeout) will have timeout
        + 1 as their data point to distinguish request errors.
        :param prunetime: Time in seconds to prune old data from the data dictionary. Setting this to 0 disables pruning.
        If not 0, data with the key of the measured timestamp - prunetime will be deleted (if it exists).
        :param floor: Whether to floor the measurement time to the minute.
        """
        self.url = url
        self.filename = filename
        self.timeout = timeout
        self.prunetime = prunetime

        with open(self.filename) as f:
            self.data = json.load(f)

        self.measuredtimestamp = int(time.time())
        if floor:
            self.measuredtimestamp = (self.measuredtimestamp // 60) * 60

    def measure(self):
        """
        Method to run the measurement.
        :return: String displaying the return time.
        """
        try:
            response = requests.get(url=self.url, timeout=self.timeout)
            finaltime = response.elapsed.total_seconds()
        except requests.exceptions.RequestException:
            finaltime = self.timeout + 1
        except:
            finaltime = self.timeout + 1

        self.__adddata(finaltime)
        try:
            self.__prunedata()
        except KeyError:
            print("Issue with prune data")
            pass

        self.__dumpdata()

        return "Measure successful, with a time of " + str(finaltime) + " seconds."

    def __adddata(self, measuredtime):
        self.data[str(self.measuredtimestamp)] = measuredtime

    def __prunedata(self):
        if self.prunetime != 0:
            keystoprune = []
            for key in self.data:
                if int(key) < int(self.measuredtimestamp - self.prunetime):
                    keystoprune.append(key)

            for key in keystoprune:
                self.data.pop(key)

    def __dumpdata(self):
        with open(self.filename, "w") as f:
            json.dump(self.data, f)


if __name__ == "__main__":
    pass
