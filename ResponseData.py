import json
from ResponseMeasure import ResponseMeasure


# Originally ResponseData was going to be its own separate thing from ResponseMeasure,
# but I subclassed since it shares some commonalities (like filename, timeout)
class ResponseData(ResponseMeasure):
    """
    ResponseData is a superclass to ResponseMeasure. It acts as a data form for the data produced by ResponseMeasure.
    """

    def __init__(self, url, filename, timeout, avgwindow, prunetime=0, floor=True):
        """
        Initialize a ResponseData object (subclass of ResponseMeasure)
        :param url: The URL of the website being requested.
        :param filename: The filename to dump the JSON data to
        :param timeout: The timeout duration for the request. Requests that fail (or truly timeout) will have timeout
        + 1 as their data point to distinguish request errors.
        :param avgwindow: The number of data points to calculate moving averages for the response data.
        :param prunetime: Time in seconds to prune old data from the data dictionary. Setting this to 0 disables pruning.
        If not 0, data with the key of the measured timestamp - prunetime will be deleted (if it exists).
        :param floor: Whether to floor the measurement time to the minute.
        """
        self.data = {}
        self.avgdata = {}
        self.keys = []
        self.avgkeys = []
        self.avgwindow = avgwindow

        super().__init__(url, filename, timeout, prunetime, floor)
        self.__processdata()

    def __processdata(self):
        """
        Private method to process response timings data after the responsedata.json file has been modified.
        :return: Nothing
        """
        self.data = {}
        self.avgdata = {}
        self.keys = []

        with open(self.filename) as f:
            self.data = json.load(f)

        self.keys = sorted(self.data.keys())

        if len(self.data) >= self.avgwindow:
            for i in range(self.avgwindow - 1, len(self.data)):
                avgvalue = 0
                for j in range(0, self.avgwindow):
                    try:
                        if self.data[self.keys[i - j]] == self.timeout + 1:
                            avgvalue = self.timeout + 1
                            break
                        avgvalue = avgvalue + self.data[self.keys[i - j]]
                    except KeyError:
                        break

                if avgvalue != 0 and avgvalue != self.timeout + 1:
                    self.avgdata[self.keys[i]] = avgvalue / self.avgwindow
                elif avgvalue == self.timeout + 1:
                    self.avgdata[self.keys[i]] = avgvalue

        self.avgkeys = sorted(self.avgdata.keys())

    def measure(self):
        super().measure()
        self.__processdata()

    def export(self, average=False):
        """
        Export the JSON dictionary for response times/average response times
        :param average: Flags if the exported dictionary will be for average response times.
        :return: A JSON dictionary of the response timings, depending on parameters passed.
        """

        if average:
            return self.avgdata
        else:
            return self.data

    def get(self, key=0, average=False, latest=False):
        """
        Get data from the response data.
        :param key: The key to get the data for.
        :param average: Flags whether to get data from the average response time dictionary
        :param latest: Flags whether to get the latest available data (ignores the value for key if this is True.)
        :return: A float indicating the response time (or average response time) for the given key.
        """
        if average:
            dicttouse = self.avgdata
        else:
            dicttouse = self.data
        key = str(key)

        if latest:
            if average:
                key = self.avgkeys[len(self.avgkeys) - 1]
            else:
                key = self.keys[len(self.keys) - 1]

        return dicttouse[key]

    def latestkey(self):
        """
        Returns the latest available timestamp as an int.
        :return: The latest available timestamp as an int.
        """

        return int(self.keys[len(self.keys) - 1])

    def latestavgkey(self):
        """
        Returns the latest available average timestamp as an int.
        :return: The latest available average timestamp as an int.
        """

        return int(self.avgkeys[len(self.avgkeys) - 1])

    def interval(self):
        """
        Returns the data interval at which data is being captured.
        This assumes the response script is being ran at consistent intervals. It captures
        the difference in key value between the last 2 values.
        :return: An integer with the difference of the last 2 values. 0 is otherwise returned if this cannot be computed
        (basically if 0 or 1 data points have been logged)
        """

        if len(self.keys) >= 2:
            return int(self.keys[len(self.keys) - 1]) - int(self.keys[len(self.keys) - 2])
        else:
            return 0