# WPI WebWork Status
Front/back-end server combo to monitor status of WPI's WebWork servers. Can be extended to monitor really any website.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

As the first major(ish) project of late 2020 where I now know more about object-oriented program design...

WPI WebWork Status Spaghet-o-meter: **1.5/10**

WPI WebWork Status is very much object oriented, with classes for the Response Measurement, then Response Data (extending Response Data), then ResponseOutage to analyze outages. The frontend does have some spaghetti, and I could definitely do some better work with the Jinja2 rendering templates. However, it's a huge improvement.

# Quick note about v2.4-v2.6
For a period of time, WPI WebWork Status was monitoring two websites, so I had to make some changes to the server setup.

With v2.6.0, we're back to monitoring one server, but there's some fragments of the modified code design to support dual-server monitoring.

# Setup
To set up WPI WebWork Status, you'll need a Python 3 install with Flask and Requests. Install via pip.

A script is included called measurement.py, which uses the ResponseMeasure class to do response measurements. It's meant to run regularly through something like cron (5 minutes is a good interval). Run this for at least 30 minutes in the standard configuration before turning on the web server component so enough data can be collected for stuff like outage analysis and average response times.

serve.py is the actual Flask server. You can serve it via WSGI (a serve.wsgi configuration file is included which works with Apache WSGI), or via Flask's built in web server.

# Usage
Visit the webpage for WebWork Status. 

There's also a link for a Siri shortcut in the source code which runs on a special method for Siri Shortcuts (/api/v1/siriShortcut). If you want to, you can clone this shortcut and set it up to use your own server.